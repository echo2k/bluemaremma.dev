��    c      4  �   L      p  &   q      �  ,   �  
   �     �     �  
   	     	     !	  '   A	  &   i	  
   �	     �	     �	  	   �	     �	     �	     �	     �	     
     
  '   '
     O
     X
     j
     �
  %   �
     �
     �
  <   �
  @     .   Y     �     �  	   �     �     �     �     �  	   �     �  *   �          8  *   T          �  =   �  &   �  *        A     J     N     e     �     �     �     �      �          "     B     V     i  
   v     �     �     �     �     �     �               7      V     w     �     �     �     �     �     �     �        -        ?     S     `  	   w     �     �     �     �      �      �  "   �     "     B  w  [  +   �  *   �  A   *  	   l  	   v     �     �     �  .   �  >   �  <   !     ^     j  #   �     �  "   �     �     �     �     �  '   �  .   $     S     Z  %   t  $   �  5   �     �  
   
  C     J   Y  8   �     �     �     �     �               .     I     Y  /   i  #   �  ,   �  <   �     '     A  L   Y  :   �  =   �  	        )     1     J     c     �     �     �  "   �     �           3     F     `  	   n     x     �  #   �     �     �     �       !   &     H  %   d     �  !   �     �     �     �                    1  7   F     ~     �     �  
   �     �     �     �               >     \     {     �        H       W   Q          5   T   .   /      +       9   L                  U       -           S   (   [       *      &           I       B           `   2   8   0   O          :   =       M              1              R             3   
   "               c   J       ,   ]      X       @   G   Z       ^       ;   )       #   a          D   _                C                P   A          4   $      	   7   <   F   K            ?   6              '   b   \   >                 V      Y   N       E   !   %       Another user already has this username Another user has the same e-mail Another user has the same username or e-mail Categories Category Disable private page Disclaimer E-Mail Error during user page creation Error inserting user data into database Error updating user data into database First name Form not found Incorrect username or password Last name Logged successfully, welcome! Login Logout Maximum Name No registration forms found One or more chosen categories are wrong Password Password strength Password used for the login Registration disclaimer Registration was successful. Welcome! Repeat password Select Sorry, you don't have the right permissions to post comments Sorry, you don't have the right permissions to view this content Sorry, your account has not been activated yet Submit Surname Telephone User E-mail User Telephone User first name User last name User name Username Username and password fields are mandatory Username or password incorrect Username used for the login WP sync - another user has the same e-mail Wrong status value You don't have a reserved area You have to set registered users default category in settings You must be logged in to post comments You must be logged in to view this content a symbol ago an uppercase character are not valid IP addresses are not valid ZIP codes are not valid dates are not valid e-mail addresses are not valid floating numbers are not valid hexadecimal colors are not valid integers are not valid telephone numbers are not valid times are not valid urls are required characters characters allowed characters and digits file size is wrong invalid data inserted is not a valid IP address is not a valid ZIP code is not a valid date is not a valid e-mail address is not a valid floating number is not a valid hexadecimal color is not a valid integer is not a valid telephone number is not a valid time is not a valid url is required login maximum maximum value is minimum value is must be accepted to proceed with registration must be at least of must be long must contain at least  no access options options allowed remember me the value doesn't match value is not between the allowed values are between the forbidden values are not between the allowed values is between the forbidden wasn't entered correctly Project-Id-Version: PrivateContent 5.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-20 20:57+0100
PO-Revision-Date: 2015-12-21 21:16+0200
Last-Translator: Daniel Mirica <mirica.daniel@gmail.com>
Language-Team: LCweb
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _;__;_e
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 1.5.7
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: classes/simple_form_validator.php
X-Poedit-SearchPath-1: classes/recaptchalib.php
X-Poedit-SearchPath-2: classes/users_manag.php
X-Poedit-SearchPath-3: classes/meta_manag.php
X-Poedit-SearchPath-4: user_registration.php
X-Poedit-SearchPath-5: user_auth.php
X-Poedit-SearchPath-6: public_api.php
X-Poedit-SearchPath-7: comment_hack.php
X-Poedit-SearchPath-8: classes/pc_form_framework.php
 Un alt utilizator foloseste deja acest nume Un alt utilizator foloseste acelasi e-mail Un alt utilizator foloseste acelasi Nume de utilizator sau E-mail Categorii Categorie Dezactivati pagina privata Precizari legale E-mail Eroare in timpul crearii paginii de utilizator Eroare la introducerea in baza de date a datelor de utilizator Eroare la actualizarea datelor de utilizator in baza de date Primul nume Nu se gaseste formular Incorect Nume utilizator sau Parola Al doilea nume Conectare reusita. Bine ai venit ! Autentificare Deconectare Maxim Nume Nu s-au gasit formulare de inregistrare Una sau mai multe categorii alese sunt gresite Parola Cat de sigura este parola Parola utilizata pentru autentificare Precizari legale pentru inregistrare Inregistrarea s-a efectuat cu succes. Bine ai venit ! Reintroduceti parola Selecteaza Ne pare rau, nu ai permisiunile necesare pentru a adauga comentarii Ne pare rau, nu ai permisiunile necesare pentru a vizualiza acest continut Ne pare rau, contul dumneavoastra nu a fost inca activat Confirma Prenume Telefon E-mail Utilizator Telefon Utilizator Prenume utilizator Nume de familie utilizator Nume Utilizator Nume utilizator Campurile Utilizator si Parola sunt obligatorii Incorect Nume utilizator sau Parola Nume Utilizator folosit pentru autentificare sincronizare WP - un alt utilizator foloseste acelasi e-mail Valoarea de stare gresita Nu aveti o zona privata Trebuie sa indicati in Setari categoria utilizatorilor inregistrati implicit Trebuie sa te autentifici pentru a putea adauga comentarii Trebuie sa fii autentificat pentru a vizualiza acest continut un simbol in urma un caracter cu majuscula nu sunt adrese IP valide nu sunt coduri postale valide nu sunt date valide nu sunt adrese de e-mail valide nu sunt numere reale valide nu sunt culori hexazecimale valide nu sunt numere intregi valide nu sunt numere de telefon valide nu sunt ore valide nu sunt adrese URL valide sunt necesare caractere caractere permise caractere si cifre dimensiunea fisierului este gresita date nevalide introduse nu este o adresa IP valida nu este un cod postal valid nu este o data valida nu este o adresa valida de e-mail nu este un numar real valid nu este o culoare hexazecimala valida nu este un numar intreg valid nu este un numar de telefon valid nu este o ora valida nu este o adresa URL valida este necesar autentificare maxim valoarea maxima este valoarea minima este trebuie sa fie acceptate pentru a efectua inregistrarea trebuie sa fie de cel putin trebuie sa fie de lungime trebuie sa contina cel putin fara acces optiuni optiuni permise memoreaza parola valoarea nu se potriveste valoarea nu este permisa intre valorile sunt interzise intre valorile nu sunt permise intre valoarea este interzisa intre nu a fost introdus corect 