=== NextGEN Facebook - Social Meta Tags for Facebook, Google, Pinterest, Twitter & More ===
Plugin Name: NextGEN Facebook (NGFB)
Plugin Slug: nextgen-facebook
Text Domain: nextgen-facebook
Domain Path: /languages
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl.txt
Donate Link: https://surniaulula.com/extend/plugins/nextgen-facebook/?utm_source=ngfb-readme-donate
Assets URI: https://surniaulula.github.io/nextgen-facebook/assets/
Tags: share, buttons, facebook, twitter, pinterest, youtube, social, sharing, meta tags, rich pin, social media, whatsapp, json, json-ld, ld+json, seo, open graph, schema, schema.org, amp, bbPress, buddypress, e-commerce, easy digital downloads, edd, g+, google, google plus, hashtags, like, linkedin, marketpress, multilingual, multisite, nextgen gallery, player card, polylang, shortcode, summary card, tumblr, twitter card, vimeo, widget, wistia, woocommerce, yotpo, yourls, the events calendar, social share, social sharing, facebook video, slideshare, video embed, social meta tags, search engine optimization, wordpress seo, yoast seo, shortlink
Contributors: jsmoriss
Requires At Least: 3.7
Tested Up To: 4.7.1
Stable Tag: 8.39.0-1

Complete meta tags for the best looking shares on Facebook, Google, Pinterest, Twitter, etc - no matter how your webpage is shared!

== Description ==

<img src="https://surniaulula.github.io/nextgen-facebook/assets/icon-256x256.png" style="width:33%;min-width:128px;max-width:256px;float:left;margin:10px 60px 40px 0;" />
<p><strong>NGFB uses your <em>existing</em> content to generate its meta tags and Schema markup</strong> &mdash; there's no need to manually enter / configure any additional settings or values, but if you want to, you can customize just about anything and everything. ;-).</p>

<p><strong>NGFB creates <em>complete</em> and <em>accurate</em> social meta tags and Schema markup</strong> &mdash; including titles, descriptions, hashtags, images, videos, ecommerce product details, author profile / authorship, co-authors, publisher information, ratings, event details, recipe information, and much more &mdash; all from your <em>existing</em> content.</p>

<p><strong>NGFB provides all the information social websites and search engines need</strong> &mdash; improving social engagement, Google Search ranking, and click-through-rates on Facebook, Google Search / Google+, Twitter, Pinterest, LinkedIn, and many more.</p>

<p><strong>NGFB includes special support and optimization features for Pinterest</strong> &mdash; providing customized Pinterest image sizes, and avoiding conflicts between incompatible Pinterest and Facebook Open Graph meta tags.</p>

<blockquote>
<p><strong>Don't need the included social share buttons?</strong></p>

<p>The <a href="https://wordpress.org/plugins/wpsso/">WordPress Social Sharing Optimization (WPSSO)</a> plugin is a fork / child of NextGEN Facebook (NGFB) &mdash; they have the same author, the same solid core features and code-base, but WPSSO is distributed without the social share buttons and their related features. Additional Free extension plugins are also available for WPSSO, including two different types of sharing button extensions:</p>

<ul>
<li><a href="https://wordpress.org/plugins/wpsso-am/">WPSSO Mobile App Meta</a></li>
<li><a href="https://wordpress.org/plugins/wpsso-organization/">WPSSO Organization Markup</a></li>
<li><a href="https://wordpress.org/plugins/wpsso-plm/">WPSSO Place / Location and Local Business Meta</a></li>
<li><a href="https://wordpress.org/plugins/wpsso-rrssb/">WPSSO Ridiculously Responsive Social Sharing Buttons</a></li>
<li><a href="https://wordpress.org/plugins/wpsso-schema-json-ld/">WPSSO Schema JSON-LD Markup</a></li>
<li><a href="https://wordpress.org/plugins/wpsso-ssb/">WPSSO Social Sharing Buttons</a></li>
<li><a href="https://wordpress.org/plugins/wpsso-strip-schema-microdata/">WPSSO Strip Schema Microdata</a></li>
<li><a href="https://wordpress.org/plugins/wpsso-tweet-a-quote/">WPSSO Tweet a Quote</a></li>
<li><a href="https://wordpress.org/plugins/wpsso-user-locale/">WPSSO User Locale</a></li>
</ul>
</blockquote>

= Quick List of Features =

**NextGEN Facebook (NGFB) Free / Basic Features**

* Adds Open Graph meta tags for Facebook, Google+, Pinterest, LinkedIn, etc.
* Adds Twitter Card meta tags:
	* [Summary Card with Large Image](https://dev.twitter.com/cards/types/summary-large-image)
	* [Summary Card](https://dev.twitter.com/cards/types/summary)
	* [Player Card](https://dev.twitter.com/cards/types/player) is available in the Pro version (requires video discovery modules, provided with the Pro version).
* Adds Weibo meta tags for article creation and update dates.
* Adds Pinterest Rich Pin meta tags and Schema [Structured Data](https://developers.google.com/search/docs/guides/intro-structured-data) markup for Google.
* Adds author (Person), publisher (Organization) and WebSite markup for Google Search and <em>Knowledge Graph</em>, including markup for the [Google Sitelinks Searchbox](https://developers.google.com/search/docs/data-types/sitelinks-searchbox).
* Customizable image dimensions for Facebook / Open Graph, Pinterest, Schema and all Twitter Card types.
* Auto-regeneration of inaccurate / missing WordPress image sizes.
* Support for Automattic's [Accelerated Mobile Pages (AMP)](https://wordpress.org/plugins/amp/) plugin.
* Support for featured, attached, gallery shortcode, and/or image HTML tags in content.
* Fallback to image alt values if the content and excerpt does not include any text.
* Optional fallback to a default image and video for index and search webpages.
* Configurable title separator character (hyphen by default).
* Configurable title and description lengths (Open Graph, Twitter Card, SEO).
* Includes author, co-author / contributor and publisher markup for Facebook, Pinterest and Google.
* Includes WordPress tags as hashtags (including a configurable maximum).
* Includes a Google / SEO description meta tag (if an SEO plugin is not detected).
* Configurable website / business social accounts for Schema markup:
	* Facebook Business Page URL
	* Google+ Business Page URL
	* Pinterest Company Page URL
	* Twitter Business @username
	* Instagram Business URL
	* LinkedIn Company Page URL
	* MySpace Business (Brand) URL
* User profile contact fields for Open Graph, Twitter Card, and Schema markup:
	* Facebook URL
	* Google+ URL
	* Instagram URL
	* LinkedIn URL
	* MySpace URL
	* Pinterest URL
	* Skype Username
	* Tumblr URL
	* Twitter @username
	* YouTube Channel URL
* Customizable **multilingual** / multi-language Site Title and Site Description texts.
* Contextual help for *every* plugin option and [comprehensive online documentation](https://surniaulula.com/codex/plugins/nextgen-facebook/).
* Uses object and transient caches to provide incredibly fast execution speeds.
* Include / exclude each social share button based on the viewing device (desktop and/or mobile).
* Social share buttons for the content, excerpt, in a widget, as a shortcode, floating sidebar, and/or PHP function.
	* Buffer
	* Email
	* Facebook
	* Google+
	* LinkedIn
	* ManageWP
	* Pinterest
	* Reddit
	* StumbleUpon
	* Tumblr
	* Twitter
	* WhatsApp (for Mobile Devices)

<blockquote>
<p>Download the Free version from <a href="https://surniaulula.github.io/nextgen-facebook/">GitHub</a> or <a href="https://wordpress.org/plugins/nextgen-facebook/">WordPress.org</a>.</p>
</blockquote>

<div style="clear:both;"></div>
<div style="width:40%;min-width:256px;max-width:1200px;height:auto;float:right;margin:10px 0 40px 60px;">
<p><img src="https://surniaulula.github.io/nextgen-facebook/images/social/twitter-player-card-single.png" width="100%" height="100%"/></p>
</div>

= Quick List of Features (Continued) =

**NextGEN Facebook (NGFB) Pro / Power-User Features**

* A Social Settings metabox for Post, Page, custom post type, user profile, and term / taxonomy (category and tag) editing pages &mdash; the Social Settings metbox allows you to customize the article topic, the shared title, the Open Graph / Facebook / Rich Pin, Google Search, and Twitter Card descriptions, along with the shared image and/or video.
* Fully renders content (including shortcodes) for accurate description texts.
* Support for embedded videos in content text (iframe and object HTML markup).
* Additional Open Graph / Rich Pin meta tags for videos and ecommerce products.
* Twitter [Player Card](https://dev.twitter.com/cards/types/player) markup for embedded videos from Facebook, Slideshare, Vimeo, Wistia, and/or Youtube.
* Include or exclude individual Google / SEO, Open Graph, Twitter Card, and Schema meta tags in the webpage head section.
* Validation of minimum / maximum image dimensions and aspect ratios.
* Configurable user profile contact field names and labels.
* Optional URL shortening with Bitly, Google, Ow.ly, TinyURL, or YOURLS.
* Dynamic button language switching based on the current WordPress locale.
* A stylesheets editor for each social share button locations (content, excerpt, shortcode, widget, etc.).
* File caching for social share button images and JavaScript, maximizing performance on VPS and dedicated hardware hosting platforms.
* Selection of preset button options by location (content, excerpt, shortcode, widget, etc.).
* Ability to include / exclude social share buttons by post type.
* Integrates with 3rd party plugins and services for additional image, video, ecommerce product details, SEO settings, etc. The following modules are included with the Pro version, and are automatically loaded if/when the supported plugins and/or services are required.
	* **Integrated 3rd Party Plugins**
		* All in One SEO Pack
		* bbPress
		* BuddyPress (including Group Forum Topics)
		* Co-Authors Plus (including Guest Authors)
		* Easy Digital Downloads
		* HeadSpace2 SEO
		* NextGEN Gallery
		* MarketPress - WordPress eCommerce
		* Polylang
		* rtMedia for WordPress, BuddyPress and bbPress
		* The Events Calendar
		* The SEO Framework
		* WooCommerce (version 1 and 2)
		* WP eCommerce
		* WordPress REST API (version 2)
		* Yoast SEO (aka WordPress SEO)
		* Yotpo Social Reviews for WooCommerce
	* **Integrated Service APIs**
		* Bitly
		* Facebook Embedded Videos
		* Google URL Shortener
		* Gravatar (Author Image)
		* Ow.ly
		* Slideshare Presentations
		* TinyURL
		* Vimeo Videos
		* Wistia Videos
		* Your Own URL Shortener (YOURLS)
		* YouTube Videos and Playlists

<blockquote>
<p><a href="https://surniaulula.com/extend/plugins/nextgen-facebook/?utm_source=ngfb-readme-purchase">Purchase the Pro version</a> (includes a <em>No Risk 30 Day Refund Policy</em>).</p>
</blockquote>

<div style="clear:both;"></div>
<div style="width:40%;min-width:256px;max-width:1200px;height:auto;float:right;margin:10px 0 40px 60px;">
<p><img src="https://surniaulula.github.io/nextgen-facebook/images/settings/ngfb-social-sharing-buttons.png" width="100%" height="100%"/></p>
</div>

= Social Sharing Buttons =

NextGEN Facebook (NGFB) comes with several social share buttons, that you can optionally include on Post / Page editing pages, above / below your content or excerpt, [bbPress](https://wordpress.org/plugins/bbpress/) single pages, [BuddyPress](https://wordpress.org/plugins/buddypress/) activity entries, as a sidebar, widget, shortcode, or even call a function from your theme template(s). Each of the following social share buttons can be enabled, configured, and styled individually:

* **Email**
* **Buffer**
* **Facebook** (Like, Send, and Share)
* **Google+**
* **LinkedIn**
* **ManageWP**
* **Pinterest**
* **Reddit**
* **StumbleUpon**
* **Tumblr** (Links, Quotes, Images, Videos)
* **Twitter**
* **WhatsApp** (for Mobile Devices)

The Facebook, Google+ and Twitter social share buttons support *multiple languages*. A default language can be chosen in the NGFB settings, and the [Pro version](https://surniaulula.com/extend/plugins/nextgen-facebook/) switches the social share button language with the webpage language / WordPress locale. NGFB can also include hashtags from WordPress and NextGEN Gallery tag names in the Open Graph (Facebook) and Pinterest Rich Pin descriptions, Tweet text, and other social captions.

<div style="clear:both;"></div>
<div style="width:40%;min-width:256px;max-width:1200px;height:auto;float:right;margin:10px 0 40px 60px;">
<p><img src="https://surniaulula.github.io/nextgen-facebook/images/social/google-social-profiles-search-results.png" width="100%" height="100%"/></p>
</div>

= Social Profiles for Google Search =

NextGEN Facebook (NGFB) includes support for <a href="https://developers.google.com/structured-data/customize/social-profiles">author (Person) and publisher (Organization) social profiles</a>, <a href="https://developers.google.com/structured-data/customize/logos">publisher (Organization) logo</a>, and <a href="https://developers.google.com/structured-data/site-name">WebSite site name</a> in Schema JSON-LD format for Google Search and their <em>Knowledge Graph</em>. The author (Person) markup includes the author's website URL, their social sharing image, and contact URLs. The publisher (Organization) markup includes the website URL, a corporate logo, the website default image, and the publisher Facebook, Google+, LinkedIn, Pinterest, and Twitter business pages.

<div style="clear:both;"></div>

= User Profile Social Contacts =

NextGEN Facebook (NGFB) Pro allows you to customize the field names, label, and add / remove the following contacts on user profile pages and [Co-Authors Plus](https://wordpress.org/plugins/co-authors-plus/) guest author profiles:

* AIM
* Facebook URL
* Google Talk
* Google+ URL
* Instagram URL
* LinkedIn URL
* MySpace URL
* Pinterest URL
* Skype Username
* Tumblr URL
* Twitter @username
* Yahoo IM
* YouTube Channel URL

= Complete Social Meta Tags =

NextGEN Facebook (NGFB) adds Facebook / Open Graph, Pinterest Rich Pins, Twitter Cards, and Search Engine Optimization (SEO) meta tags to the head section of webpages. These meta tags are used by Google Search and all social websites to describe and display your content correctly (title, description, hashtags, images, videos, ecommerce product details, author profile / authorship, publisher, etc.). NGFB uses the *existing* content of your webpages to build HTML meta tags &mdash; there's no need to manually enter / configure any additional values or settings (although many settings and options *are* available in the Pro version). <a href="https://surniaulula.com/extend/plugins/nextgen-facebook/screenshots/">See example screenshots from Google Search, Google+, Facebook, Twitter, Pinterest, StumbleUpon, Tumblr, etc.</a>.

NextGEN Facebook (NGFB) provides the [Summary](https://dev.twitter.com/cards/types/summary), [Summary with Large Image](https://dev.twitter.com/cards/types/summary-large-image), and [Player](https://dev.twitter.com/cards/types/player) Twitter Cards &mdash; *including configurable image sizes for each card type*.

= 3rd Party Integration (Pro version) =

<div style="clear:both;"></div>
<div style="width:40%;min-width:256px;max-width:1200px;height:auto;float:right;margin:10px 0 40px 60px;">
<p><img src="https://surniaulula.github.io/nextgen-facebook/images/social/twitter-large-image-summary-single.png" width="100%" height="100%"/></p>
</div>

**Images and Videos**

NextGEN Facebook (NGFB) detects and uses all images - associated or included in your content - including the WordPress image gallery and [NextGEN Gallery](https://wordpress.org/plugins/nextgen-gallery/) shortcodes. WordPress Media Library images (and NextGEN Gallery in the Pro version) are resized according to their intended consumer (Facebook, Twitter, Pinterest, etc). The Pro version detects embedded videos from Facebook, Slideshare, Vimeo, Wistia, and Youtube (including preview image, video title, and video description). NGFB (Pro version) also includes support for [NextGEN Gallery](https://wordpress.org/plugins/nextgen-gallery/) albums, galleries and images (shortcodes, image tags, album / gallery preview images, etc.).

**Enhanced SEO**

NextGEN Facebook (NGFB) Pro integrates with [All in One SEO Pack](https://wordpress.org/plugins/all-in-one-seo-pack/), [HeadSpace2 SEO](https://wordpress.org/plugins/headspace2/), [The SEO Framework](https://wordpress.org/plugins/autodescription/), and [Yoast SEO](https://wordpress.org/plugins/wordpress-seo/) (aka WordPress SEO) &mdash; making sure your custom SEO settings are reflected in the Open Graph, Rich Pin, Schema Structured Data, and Twitter Card meta tags.

<div style="clear:both;"></div>
<div style="width:40%;min-width:256px;max-width:1200px;height:auto;float:right;margin:10px 0 40px 60px;">
<p><img src="https://surniaulula.github.io/nextgen-facebook/images/social/twitter-product-ngfb-single.png" width="100%" height="100%"/></p>
</div>

**eCommerce Products**

NextGEN Facebook (NGFB) Pro also supports [Easy Digital Downloads](https://wordpress.org/plugins/easy-digital-downloads/), [MarketPress - WordPress eCommerce](https://wordpress.org/plugins/wordpress-ecommerce/), [WooCommerce](https://wordpress.org/plugins/woocommerce/), and [WP e-Commerce](https://wordpress.org/plugins/wp-e-commerce/) product pages, creating appropriate meta tags for [Facebook Products](https://developers.facebook.com/docs/payments/product/, and [Pinterest Rich Pins](http://developers.pinterest.com/rich_pins/), including variations and additional / custom images.

**Forums and Social**

NextGEN Facebook (NGFB) Pro supports [bbPress](https://wordpress.org/plugins/bbpress/), [BuddyPress](https://wordpress.org/plugins/buddypress/) (see the [BuddyPress Integration Notes](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/buddypress-integration/)), and [rtMedia for WordPress, BuddyPress and bbPress](https://wordpress.org/plugins/buddypress-media/), making sure your meta tags reflect the page content, including appropriate titles, descriptions, images, author information, etc. Social share buttons can also be added to [bbPress](https://wordpress.org/plugins/bbpress/) single template pages and [BuddyPress](https://wordpress.org/plugins/buddypress/) activities.

= Proven Performance =

NextGEN Facebook (NGFB) is *fast and coded for performance*, making full use of all available caching techniques (persistent / non-persistent object and disk caching). NGFB loads only the library files and object classes it needs, keeping it small, fast, and yet still able to support a wide range of 3rd party integration features. NGFB requires PHP v5.2.0 or better, and is fully compatible with PHP v7.

= Professional Support =

NextGEN Facebook (NGFB) support and development is on-going. You can review the [FAQ](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/) and [Notes](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/) pages for additional setup information. If you have any suggestions or comments, post them to the [WordPress support forum](https://wordpress.org/support/plugin/nextgen-facebook) or the [Pro version support website](http://nextgen-facebook.support.surniaulula.com/).

Follow Surnia Ulula on [Google+](https://plus.google.com/+SurniaUlula/?rel=author), [Facebook](https://www.facebook.com/SurniaUlulaCom), [Twitter](https://twitter.com/surniaululacom), and [YouTube](http://www.youtube.com/user/SurniaUlulaCom).

== Installation ==

= Install and Uninstall =

* [Install the Plugin](https://surniaulula.com/codex/plugins/nextgen-facebook/installation-to/install-the-plugin/)
* [Integration Notes](https://surniaulula.com/codex/plugins/nextgen-facebook/installation/integration/)
	* [BuddyPress Integration](https://surniaulula.com/codex/plugins/nextgen-facebook/installation/integration/buddypress-integration/)
* [Uninstall the Plugin](https://surniaulula.com/codex/plugins/nextgen-facebook/installation/uninstall-the-plugin/)
* [Performance Tuning](https://surniaulula.com/codex/plugins/nextgen-facebook/installation/performance-tuning/)
* [Debugging and Problem Solving](https://surniaulula.com/codex/plugins/nextgen-facebook/installation/debugging-and-problem-solving/)
* [Developer Special - Buy one, Get one Free](https://surniaulula.com/codex/plugins/nextgen-facebook/installation/developer-special-buy-one-get-one-free/)

= Plugin Setup =

* [A Setup Guide for NGFB](https://surniaulula.com/codex/plugins/nextgen-facebook/installation/a-setup-guide/)

== Frequently Asked Questions ==

= Frequently Asked Questions =

* [Can I use the Pro version on multiple websites?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/can-i-use-the-pro-version-on-multiple-websites/)
* [Does LinkedIn read the Open Graph meta tags?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/does-linkedin-read-the-open-graph-meta-tags/)
* [Doesn't an SEO plugin cover that?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/doesnt-an-seo-plugin-cover-that/)
* [How can I exclude / ignore certain parts of the content text?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-exclude-certain-parts-of-the-content-text/)
* [How can I fix a ERR_TOO_MANY_REDIRECTS error?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-fix-a-err_too_many_redirects-error/)
* [How can I fix an "HTTP error" when uploading images?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-fix-an-http-error-when-uploading-images/)
* [How can I have smaller dimensions for the default image?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-have-smaller-dimensions-for-the-default-image/)
* [How can I see what the Facebook crawler sees?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-see-what-the-facebook-crawler-sees/)
* [How can I share a single NextGEN Gallery image?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-share-a-single-nextgen-gallery-image/)
* [How do I attach an image without showing it on the webpage?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-do-i-attach-an-image-without-showing-it-on-the-webpage/)
* [How do I fix my theme's front page pagination?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-do-i-fix-my-themes-front-page-pagination/)
* [How do I install the NGFB Pro version?](https://wpsso.com/codex/plugins/wpsso/faq/how-do-i-install-the-ngfb-pro-version/)
* [How do I remove duplicate meta tags?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-do-i-remove-duplicate-meta-tags/)
* [How does NGFB find / detect / select images?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-does-ngfb-find-detect-select-images/)
* [Social Sharing Buttons](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/)
	* [Can I share a single image on a webpage?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/can-i-share-a-single-image-on-a-webpage/)
	* [Can I use other social sharing buttons?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/can-i-use-other-social-sharing-buttons/)
	* [How do I turn on Social Sharing Buttons for a page?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/how-do-i-turn-on-social-sharing-buttons-for-a-page/)
	* [Why are the buttons showing the wrong language?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/why-are-the-buttons-showing-the-wrong-language/)
	* [Why does the Facebook "Like" button flyout get clipped?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/why-does-the-facebook-like-button-flyout-get-clipped/)
	* [Why doesn't the Pinterest button show?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/why-doesnt-the-pinterest-button-show/)
	* [Why doesn't the Twitter count increase?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/why-doesnt-the-twitter-count-increase/)
* [W3C says "there is no attribute 'property'"](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/w3c-says-there-is-no-attribute-property/)
* [What about Google Search and Google Plus?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/what-about-google-search-and-google-plus/)
* [What features of NextGEN Gallery are supported?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/what-features-of-nextgen-gallery-are-supported/)
* [What is the difference between NGFB and WPSSO?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/what-is-the-difference-between-ngfb-and-wpsso/)
* [What is the difference between the Free and Pro versions?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/what-is-the-difference-between-the-free-and-pro-versions/)
* [Why aren't Pins from my website posting Rich?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-arent-pins-from-my-website-posting-rich/)
* [Why do my Facebook shares have small images?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-do-my-facebook-shares-have-small-images/)
* [Why does Facebook play videos instead of linking them?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-does-facebook-play-videos-instead-of-linking-them/)
* [Why does Facebook show the wrong image / text?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-does-facebook-show-the-wrong-image-text/)
* [Why does Google Structured Data Testing Tool show errors?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-does-google-structured-data-testing-tool-show-errors/)
* [Why does the plugin ignore some &lt;img/&gt; HTML tags?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-does-ngfb-ignore-some-img-html-tags/)
* [Why don't my Twitter Cards show on Twitter?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-dont-my-twitter-cards-show-on-twitter/)
* [Why is the Open Graph title the same for every webpage?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-is-the-open-graph-title-the-same-for-every-webpage/)
* [Why is the page blank or its components misaligned?](https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-is-the-page-blank-or-its-components-misaligned/)

== Other Notes ==

= Additional Documentation =

* [About NGFB Pro Integration Modules](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/)
	* [Author Gravatar](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/author-gravatar/)
	* [Easy Digital Downloads](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/easy-digital-downloads/)
	* [Facebook, Slideshare, Vimeo, Wistia, Youtube Video APIs](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/facebook-slideshare-vimeo-wistia-youtube-video-apis/)
	* [HeadSpace2 SEO](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/headspace2-seo/)
	* [WooCommerce](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/woocommerce/)
	* [WordPress REST API version 2](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/wordpress-rest-api-v2/)
* [Contact Information and Feeds](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/contact-information/)
* [Developer Resources](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/)
	* [Constants](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/constants/)
		* [Disable the Open Graph Meta Tags](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/constants/disable-the-open-graph-meta-tags/)
	* [Filters](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/)
		* [Filter Examples](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/)
			* [Add Schema aggregateRating Meta Tags](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/add-schema-aggregaterating-meta-tags/)
			* [Detect YouTube URL Links as Videos](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/detect-youtube-url-links-as-videos/)
			* [Modify Shortcode Attributes (URL)](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/modify-shortcode-attributes-url/)
			* [Modify the "article:tag" Keywords / Names](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/modify-the-articletag-keywords-names/)
			* [Modify the Default Article Topics List](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/modify-the-default-topics-list/)
			* [Modify the Home Page Title for Facebook / Open Graph](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/modify-the-home-page-title-for-facebook-open-graph/)
		* [Filters by Category](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/)
			* [Head Filters](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/head/)
			* [Media Filters](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/media/)
			* [Open Graph Filters](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/open-graph/)
			* [Twitter Card Filters](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/twitter-card/)
			* [Webpage Filters](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/webpage/)
		* [Filters by Name](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-name/)
		* [Other Filters](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/other/)
	* [Sharing Buttons Function](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/sharing-buttons-function/)
	* [The $mod Variable](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/the-mod-variable/)
* [Inline Variables](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/inline-variables/)
* [Multisite / Network Support](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/multisite-network-support/)
* [NGFB Shortcode for Sharing Buttons](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/ngfb-shortcode/)
* [Styling Social Buttons](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/styling-social-buttons/)
* [Working with Image Attachments](https://surniaulula.com/codex/plugins/nextgen-facebook/notes/working-with-image-attachments/)

== Screenshots ==

01. NGFB Free Essential Settings &mdash; includes site information, Facebook / Open Graph, Google / Schema, Pinterest, and Twitter options. Additional options are available in General and Advanced settings pages.
02. NGFB (Pro version) Social Settings on Posts, Pages, Taxonomy / Terms, and User editing pages &mdash; allows you to modify the default title, description, image, video, preview an example share, preview the meta tags, and validate webpage markup with online tools.
03. Example Facebook Link Share.
04. Example Facebook Video Share (Pro version required).
05. Example Twitter 'Summary' Card.
06. Example Twitter 'Large Image Summary' Card.
07. Example Twitter 'Player' Card (Pro version required).
08. Example Pinterest Product Pin (Zoomed).
09. Example Social Profiles in Google Search &mdash; NGFB includes author (Person) and publisher (Organization) social profiles in schema.org JSON-LD format for Google Search and their Knowledge Graph.
10. Example Showing All Social Sharing Buttons Enabled.

== Changelog ==

<blockquote class="top_info">
<p>New versions of the plugin are released approximately every week (more or less). New features are added, tested, and released incrementally, instead of grouping them together in a major version release. When minor bugs fixes and/or code improvements are applied, new versions are also released. This release schedule keeps the code stable and reliable, at the cost of more frequent updates.</p>

<p>See <a href="https://en.wikipedia.org/wiki/Release_early,_release_often">release early, release often (RERO) software development philosophy</a> on Wikipedia for more information on the benefits of smaller / more frequent releases.</p>
</blockquote>

= Free / Basic Version Repository =

* [GitHub](https://surniaulula.github.io/nextgen-facebook/)
* [WordPress.org](https://wordpress.org/plugins/nextgen-facebook/developers/)

= Version Numbering Scheme =

Version components: `{major}.{minor}.{bugfix}-{stage}{level}`

* {major} = Major code changes / re-writes or significant feature changes.
* {minor} = New features / options were added or improved.
* {bugfix} = Bugfixes or minor improvements.
* {stage}{level} = dev &lt; a (alpha) &lt; b (beta) &lt; rc (release candidate) &lt; # (production).

Note that the production stage level can be incremented on occasion for simple text revisions and/or translation updates. See [PHP's version_compare()](http://php.net/manual/en/function.version-compare.php) documentation for additional information on "PHP-standardized" version numbering.

= Changelog / Release Notes =

**Version 8.39.1-dev2 (2017/01/19)**

* *New Features*
	* None
* *Improvements*
	* Improved the loading speed of editing table column content (NGFB Schema, NGFB Img, and NGFB Desc columns).
	* Refactored the set_force_regen() and is_force_regen() methods to use a single transient array (to reduce the number of transient queries).
* *Bugfixes*
	* Added a missing user ID argument when getting the user object in the SucomWebpage get_description() method.
	* Updated delete_expired_db_transients() to remove transients without expiration times.
* *Developer Notes*
	* Added a new is_force_regen() method in the NgfbUtil class to check if the forced regen transient is set, along with a set_force_regen() method to set the transient, and get_force_regen_id() to return the transient key ID.
	* Added a new is_post_exists() static method in the SucomUtil class to check if a post ID exists or not.
	* Added support for post ID checking in the is_post_page() method in the SucomUtil class.
	* Renamed the 'schema_id' sortable column and meta name to 'schema_type' (including related filter hooks).
	* Replaced column content creation filters / methods by 'get_{post|term|user}_metadata' filter hooks to return pre-generated custom meta values instead.
	* Removed the 'ngfb_cache_expire_column_content' filter (a transient is no longer required for column content).

**Version 8.39.0-1 (2017/01/13)**

* *New Features*
	* Added a new "Add NGFB Schema Column in" option (for posts / pages, terms, and users) in the NGFB Advanced settings page (enabled by default).
* *Improvements*
	* None
* *Bugfixes*
	* None
* *Developer Notes*
	* Added a new SucomUtil get_dashicons() static method to retrieve a sorted/unsorted dashicons array.
	* Re-added code to support the Yoast_Notification_Center class from older Yoast SEO versions.

**Version 8.38.3-1 (2017/01/08)**

* *New Features*
	* None
* *Improvements*
	* Added both parent and child theme header template files to the head HTML element attributes check.
	* The extensions listed on the "Extension Plugins and Pro Licenses" settings page are now sorted by name.
* *Bugfixes*
	* None
* *Developer Notes*
	* Refactored the SucomUtil is_https() method and added a check for proxy / load-balancing 'HTTP_X_FORWARDED_PROTO' and 'HTTP_X_FORWARDED_SSL' web server variables.
	* Refactored the SucomUtil get_prot() method to support the FORCE_SSL and FORCE_SSL_ADMIN constants.

**Version 8.38.2-1 (2017/01/02)**

* *New Features*
	* None
* *Improvements*
	* Added a new "Honor the FORCE_SSL Constant" option on the Advanced settings page.
* *Bugfixes*
	* None
* *Developer Notes*
	* Added a check and action hook to honor the FORCE_SSL constant on the front-end.

**Version 8.38.1-1 (2016/12/28)**

* *New Features*
	* None
* *Improvements*
	* The "Read Yoast SEO Custom Meta" option is now enabled by default, if the Yoast SEO plugin is active or its settings are found in the database, otherwise it is disabled by default (Pro version).
* *Bugfixes*
	* None
* *Developer Notes*
	* Added a new NgfbFilters class to centralize 3rd party plugin filter hooks.
	* Added a new NgfbUtil get_canonical_url() method specifically for the canonical meta tag.
	* Added a new 'ngfb_canonical_url' filter to allow filtering of canonical and sharing URLs separately.

**Version 8.38.0-1 (2016/12/24)**

* *New Features*
	* Added a new "Read Yoast SEO Social Meta" option for Yoast SEO users under the NGFB &gt; Advanced &gt; Social / Custom Meta tab (Pro version).
	* Added a new "Custom Facebook Locale" option in the NGFB &gt; General settings page to customize the WordPress locale value for Facebook.
* *Improvements*
	* Removed the "Verify Peer SSL Certificate" option as this feature should always be enabled anyway.
* *Bugfixes*
	* Fixed fetching of image size for a custom Schema image URL.
* *Developer Notes*
	* Removed the share/curl/ca-bundle.crt file to use the wordpress/wp-includes/certificates/ca-bundle.crt file instead.
	* Added a check for positive numbers when adding Schema image size properties.

**Version 8.37.8-1 (2016/12/12)**

* *New Features*
	* None
* *Improvements*
	* None
* *Bugfixes*
	* Fixed the Reddit button title by adding the title non-encoded.
* *Developer Notes*
	* Renamed the `NGFB_OPTIONS_#_*` multisite single-option constants to `NGFB_ID_#_OPT_*` and optimized their checks.
	* Added a new SucomUtil explode_csv() static method to explode and trim CSV strings.

**Version 8.37.7-1 (2016/12/08)**

* *New Features*
	* None
* *Improvements*
	* Added support for the new get_user_locale() function in WordPress v4.7.
	* Updated the Facebook share button with latest Facebook options (see https://developers.facebook.com/docs/plugins/share-button for details).
		* Markup Language
		* Button Layout
		* Button Size
		* Mobile iFrame
	* Removed the Facebook "Default Content Language" option from the Essential and General settings pages (not required for the og:locale value).
* *Bugfixes*
	* Fixed the "Click here update header templates automatically" URL in the notice message.
* *Developer Notes*
	* None

**Version 8.37.6-3 (2016/12/05)**

* *New Features*
	* None
* *Improvements*
	* Optimized wp_cache and transient caching for multilingual sites.
	* Added new Schema meta tags:
		* alternatename
		* email
		* telephone
		* currenciesaccepted
		* paymentaccepted
		* pricerange 
		* preptime
		* cooktime
* *Bugfixes*
	* Fixed the sharing URL value for BuddyPress users (Pro version).
* *Developer Notes*
	* Added an SucomUtil update_transient_array() method to update transient arrays and keep the original transient expiration time.
	* Added a new 'ngfb_json_prop_https_schema_org_potentialaction' filter.
	* Removed $locale argument from SucomUtil get_mod_salt() calls.

**Version 8.37.5-1 (2016/11/28)**

* *New Features*
	* None
* *Improvements*
	* None
* *Bugfixes*
	* Fixed BuddyPress user page detection when current object is a post (Pro version).
* *Developer Notes*
	* Added extra debugging messages in the BuddyPress user class methods (Pro version).

**Version 8.37.4-1 (2016/11/25)**

* *New Features*
	* None
* *Improvements*
	* None
* *Bugfixes*
	* Fixed an incorrect variable name in the WhatsApp class which prevented the button HTML from being added.
	* Fixed the Tumblr caption, title, and description attribute values that were being added without being properly encoded.
* *Developer Notes*
	* Added a check for 'manage_options' permission before checking for outdated WP / PHP versions and duplicate post meta tags. 
	* Removed a call to stripslashes() when saving HTML / CSS / JS code.
	* Refactored the Twitter button class to use the $mod variable instead of the global $post.
	* Renamed the 'buttons_use_social_css' option key to 'buttons_use_social_style'.
	* Renamed the 'buttons_enqueue_social_css' option key to 'buttons_enqueue_social_style'.

**Version 8.37.3-1 (2016/11/17)**

* *New Features*
	* None
* *Improvements*
	* Updated the "Plugin Setup Guide and Notes".
	* Updated hard minimum and recommended minimum WordPress and PHP versions.
		* WordPress v3.7 (hard minimum) and v4.2 (recommended minimum). See [WordPress Supported Versions](https://codex.wordpress.org/Supported_Versions) for details.
		* PHP v5.2 (hard minimum) and v5.4 (recommended minimum). See [PHP Supported Versions](http://php.net/supported-versions.php) for details.
	* Added a "Reference URL" link to notice messages when generating the head meta tag array.
* *Bugfixes*
	* None
* *Developer Notes*
	* Added a sharing URL argument to the SucomUtil get_mod_salt() method for cases where the $mod id is false.
	* Renamed the 'ngfb_json_array_schema_type_ids' filter to 'ngfb_json_array_schema_page_type_ids' to emulate the $page_type_id variable name.
	* Removed the deprecated $use_post argument from the get_array() methods for the Open Graph, Twitter Card, and Weibo classes.

**Version 8.37.2-1 (2016/11/12)**

* *New Features*
	* None
* *Improvements*
	* Added an "Item Type for Blog Home Page" option for non-static home pages.
	* Simplified the Schema mainEntityOfPage markup property by using a URL instead of an @id.
* *Bugfixes*
	* None
* *Developer Notes*
	* Refactored the NgfbSchema class to provide a public get_json_data() method for other classes.
	* Added NgfbPost get_posts(), NgfbTerm get_posts(), and NgfbUser get_posts() methods.
	* Added a NgfbMeta get_posts_mods() method to return `$mod` arrays for all posts in the current archive page.
	* Added a NgfbSchema get_json_data() method used by get_json_array() in the same class.
	* Renamed the NgfbSchema get_head_item_type() method to get_mod_schema_type().
	* Renamed the NgfbSchema get_item_type_context() method to get_schema_type_context().

**Version 8.37.0-1 (2016/11/04)**

* *New Features*
	* Replaced the Object Cache Expiry option with new options for finer control of caching features.
		* Head Markup Array Cache Expiry (default 3 days).
		* Shortened URL Cache Expiry (default 1 week).
		* List Column Content Cache Expiry (default 1 week).
		* Filtered Content Text Cache Expiry (default 1 hour).
		* Get Image (URL) Size Cache Expiry (default 1 day).
		* Article Topics Array Cache Expiry (default 4 weeks).
		* Schema Types Array Cache Expiry (default 4 weeks).
		* Sharing Buttons Cache Expiry (default 1 week).
* *Improvements*
	* Replaced the "Social File Cache Expiry" drop-down (in hours) with an input field (in seconds).
* *Bugfixes*
	* Fixed the SucomUtil get_first_last_next_nums() method, which was not returning a correct 'next' number for single element arrays.
* *Developer Notes*
	* Refactored the NgfbShortcodeSharing and NgfbWidgetSharing classes to optimize caching performance.
	* Refactored the ngfb_get_sharing_buttons() function to optimize caching performance.
	* Added a new NgfbSharing get_buttons_cache_index() method to cache social sharing buttons HTML as array elements. This optimizes the cache by storing one transient instead of one transient per social sharing buttons location within the same webpage.
	* Refactored the NgfbSchema get_json_array() method for a slight performance improvement.
	* Added https://schema.org/Thing to the Schema Types array (as top-level parent for all other Schema types).
	* Replaced the `$user_id` argument in all JSON data and property filters by the Schema `$type_id`.
	* Renamed the 'ngfb_json_data_https_schema_org' filter to 'ngfb_json_data_https_schema_org_thing'.
	* Renamed the 'ngfb_json_array_type_ids' filter to 'ngfb_json_array_schema_type_ids'.
	* Removed the 'ngfb_add_json_https_schema_org' filter.
	* Renamed the 'ngfb_shortcode_ngfb' filter to 'ngfb_sharing_shortcode_ngfb'.
	* Renamed the NGFB_META_TAGS_DISABLE contant to NGFB_HEAD_HTML_DISABLE.
	* Renamed the NGFB_SHARING_SHORTCODE contant to NGFB_SHARING_SHORTCODE_NAME.
	* Removed the NGFB_TRANSIENT_CACHE_DISABLE, NGFB_OBJECT_CACHE_DISABLE and NGFB_FILE_CACHE_DISABLE constants.
	* Added several new filters to adjust transient / object cache expiration:
		* 'ngfb_cache_expire_article_topics' ( $secs );
		* 'ngfb_cache_expire_column_content' ( $secs );
		* 'ngfb_cache_expire_content_text' ( $secs );
		* 'ngfb_cache_expire_head_array' ( $secs );
		* 'ngfb_cache_expire_image_url_size' ( $secs );
		* 'ngfb_cache_expire_schema_types' ( $secs );
		* 'ngfb_cache_expire_sharing_buttons' ( $secs );
		* 'ngfb_cache_expire_shorten_url' ( $secs );	// Pro version
		* 'ngfb_cache_expire_social_file' ( $secs );

**Version 8.36.3-2 (2016/10/23)**

* *New Features*
	* None
* *Improvements*
	* None
* *Bugfixes*
	* Fixed HTML table syntax in the Tumblr settings metabox.
* *Developer Notes*
	* Minimum requirements updated to WP v3.5 and PHP v5.4.
	* Renamed the Social Settings 'header' index name to 'text'.
	* Renamed the 'wpsso_admin_post_header' filter to 'wpsso_admin_post_head'.
	* Renamed the NgfbHead get_header_array() method to get_head_array().
	* Added a new NgfbHead get_head_index() method to cache meta tags as array elements. This optimizes the head meta transient cache for Pinterest by storing 1 transient instead of 2.

**Version 8.36.2-1 (2016/10/17)**

* *New Features*
	* None
* *Improvements*
	* Changed all http://surniaulula.com/ URLs to https.
	* Added transient caching of results when fetching size information for image URLs.
* *Bugfixes*
	* None
* *Developer Notes*
	* None

**Version 8.36.0-1 (2016/10/15)**

* *New Features*
	* Added new Item Type options under the Google / Schema tab in the General settings page:
		* Item Type for Archive Page (default is https://schema.org/CollectionPage)
		* Item Type for User / Author Page (default is https://schema.org/ProfilePage)
		* Item Type for Search Results Page (default is https://schema.org/SearchResultsPage)
	* Added a "Validate AMP Markup" button under the Social Settings metabox Validate tab for the AMP Validator.
	* Added a "Validate HTML Markup" button under the Social Settings metabox Validate tab for the W3C Markup Validation service.
	* Added a configurable length warning for the Open Graph description textarea (defaults is 300 characters hard limit, and 200 characters soft limit).
* *Improvements*
	* Added the https://schema.org/WebPage sub-types in the plugin config schema_type array (AboutPage, CheckoutPage, CollectionPage, ContactPage, ItemPage, ProfilePage, QAPage, and SearchResultsPage).
* *Bugfixes*
	* Fixed getting the term object (category and tags) when no term ID is provided to the SucomUtil get_term_object() method.
	* Removed the Open Graph namespace from the HTML tag attributes when using the AMP plugin.
* *Developer Notes*
	* Removed the NGFB_DEFAULT_AUTHOR_OPTIONS constant, and all associated default author options.
	* Renamed all http://schema.org URLs to https://schema.org and all http_schema_org filters to https_schema_org.

== Upgrade Notice ==

= 8.39.1-dev2 =

(2017/01/19) Improved the loading speed of editing table column content (NGFB Schema, NGFB Img, and NGFB Desc columns).

= 8.39.0-1 =

(2017/01/13) Added a new "Add NGFB Schema Column in" option for posts / pages, terms, and users edit tables. Re-added code to support the Yoast_Notification_Center class from older Yoast SEO versions.

= 8.38.3-1 =

(2017/01/08) Added both parent and child theme header template files to the head HTML element attributes check. Refactored the SucomUtil is_https() and get_prot() methods.

= 8.38.2-1 =

(2017/01/02) Added a new "Honor the FORCE_SSL Constant" option on the Advanced settings page.

= 8.38.1-1 =

(2016/12/28) Added an NgfbFilters class to centralize 3rd party plugin filter hooks. Added a new NgfbUtil get_canonical_url() method and 'ngfb_canonical_url' filter.

= 8.38.0-1 =

(2016/12/24) Fixed fetching of image size for a custom Schema image URL. Removed the share/curl/ca-bundle.crt file to use the WordPress ca-bundle.crt file instead. Added new "Read Yoast SEO Social Meta" and "Custom Facebook Locale" options.

= 8.37.8-1 =

(2016/12/12) Fixed the Reddit button title by adding the title non-encoded. Renamed the multisite single-option constants and optimized their checks. Added a new SucomUtil explode_csv() static method.

= 8.37.7-1 =

(2016/12/08) Added support for the new get_user_locale() function in WordPress v4.7. Updated the Facebook share button with latest Facebook options. Fixed the "Click here update header templates automatically" URL in the notice message.

= 8.37.6-3 =

(2016/12/05) Fixed the sharing URL value for BuddyPress users (Pro version). Optimized wp_cache and transient caching for multilingual sites. Added a new method to update transient arrays and keep the original transient expiration time. Added several new Schema meta tags.

= 8.37.5-1 =

(2016/11/28) Fixed BuddyPress user page detection when current object is a post (Pro version).

= 8.37.4-1 =

(2016/11/25) Added a check for 'manage_options' permission before checking for outdated WP / PHP versions and duplicate post meta tags. Fixed an incorrect variable name in the WhatsApp class. Fixed the Tumblr caption, title, and description attribute values.

= 8.37.3-1 =

(2016/11/17) Updated the "Plugin Setup Guide and Notes". Updated hard minimum and recommended minimum WordPress and PHP versions. Added a "Reference URL" link to notice messages when generating the head meta tag array.

= 8.37.2-1 =

(2016/11/12) Refactored the NgfbSchema class to provide a public get_json_data() method for other classes. Added an "Item Type for Blog Home Page" option for non-static home pages. Simplified the Schema mainEntityOfPage markup property by using a URL instead of an @id.

= 8.37.0-1 =

(2016/11/03) Replaced the Object Cache Expiry option with new options for finer control of caching features. Refactored the Schema JSON method for a slight performance improvement. Added https://schema.org/Thing to the Schema Types array. Optimized the sharing buttons HTML cache by storing only one transient per webpage.

