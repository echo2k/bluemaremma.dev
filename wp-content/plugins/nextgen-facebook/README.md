<h1>NextGEN Facebook</h1><h3>Social Meta Tags for Facebook, Google, Pinterest, Twitter &amp; More</h3>

<table>
<tr><th align="right" valign="top" nowrap>Plugin Name</th><td>NextGEN Facebook (NGFB)</td></tr>
<tr><th align="right" valign="top" nowrap>Summary</th><td>Complete meta tags for the best looking shares on Facebook, Google, Pinterest, Twitter, etc - no matter how your webpage is shared!</td></tr>
<tr><th align="right" valign="top" nowrap>Stable Version</th><td>8.39.0-1</td></tr>
<tr><th align="right" valign="top" nowrap>Requires At Least</th><td>WordPress 3.7</td></tr>
<tr><th align="right" valign="top" nowrap>Tested Up To</th><td>WordPress 4.7.1</td></tr>
<tr><th align="right" valign="top" nowrap>Contributors</th><td>jsmoriss</td></tr>
<tr><th align="right" valign="top" nowrap>Website URL</th><td><a href="https://surniaulula.com/extend/plugins/nextgen-facebook/?utm_source=ngfb-readme-donate">https://surniaulula.com/extend/plugins/nextgen-facebook/?utm_source=ngfb-readme-donate</a></td></tr>
<tr><th align="right" valign="top" nowrap>License</th><td><a href="https://www.gnu.org/licenses/gpl.txt">GPLv3</a></td></tr>
<tr><th align="right" valign="top" nowrap>Tags / Keywords</th><td>share, buttons, facebook, twitter, pinterest, youtube, social, sharing, meta tags, rich pin, social media, whatsapp, json, json-ld, ld+json, seo, open graph, schema, schema.org, amp, bbPress, buddypress, e-commerce, easy digital downloads, edd, g+, google, google plus, hashtags, like, linkedin, marketpress, multilingual, multisite, nextgen gallery, player card, polylang, shortcode, summary card, tumblr, twitter card, vimeo, widget, wistia, woocommerce, yotpo, yourls, the events calendar, social share, social sharing, facebook video, slideshare, video embed, social meta tags, search engine optimization, wordpress seo, yoast seo, shortlink</td></tr>
</table>

<h2>Description</h2>

<p align="center"><img src="https://surniaulula.github.io/nextgen-facebook/assets/icon-256x256.png" /></p><p></p>

<p><strong>NGFB uses your <em>existing</em> content to generate its meta tags and Schema markup</strong> &mdash; there's no need to manually enter / configure any additional settings or values, but if you want to, you can customize just about anything and everything. ;-).</p>

<p><strong>NGFB creates <em>complete</em> and <em>accurate</em> social meta tags and Schema markup</strong> &mdash; including titles, descriptions, hashtags, images, videos, ecommerce product details, author profile / authorship, co-authors, publisher information, ratings, event details, recipe information, and much more &mdash; all from your <em>existing</em> content.</p>

<p><strong>NGFB provides all the information social websites and search engines need</strong> &mdash; improving social engagement, Google Search ranking, and click-through-rates on Facebook, Google Search / Google+, Twitter, Pinterest, LinkedIn, and many more.</p>

<p><strong>NGFB includes special support and optimization features for Pinterest</strong> &mdash; providing customized Pinterest image sizes, and avoiding conflicts between incompatible Pinterest and Facebook Open Graph meta tags.</p>

<blockquote>
<p><strong>Don't need the included social share buttons?</strong></p>

<p>The <a href="https://wordpress.org/plugins/wpsso/">WordPress Social Sharing Optimization (WPSSO)</a> plugin is a fork / child of NextGEN Facebook (NGFB) &mdash; they have the same author, the same solid core features and code-base, but WPSSO is distributed without the social share buttons and their related features. Additional Free extension plugins are also available for WPSSO, including two different types of sharing button extensions:</p>

<ul>
<li><a href="https://wordpress.org/plugins/wpsso-am/">WPSSO Mobile App Meta</a></li>
<li><a href="https://wordpress.org/plugins/wpsso-organization/">WPSSO Organization Markup</a></li>
<li><a href="https://wordpress.org/plugins/wpsso-plm/">WPSSO Place / Location and Local Business Meta</a></li>
<li><a href="https://wordpress.org/plugins/wpsso-rrssb/">WPSSO Ridiculously Responsive Social Sharing Buttons</a></li>
<li><a href="https://wordpress.org/plugins/wpsso-schema-json-ld/">WPSSO Schema JSON-LD Markup</a></li>
<li><a href="https://wordpress.org/plugins/wpsso-ssb/">WPSSO Social Sharing Buttons</a></li>
<li><a href="https://wordpress.org/plugins/wpsso-strip-schema-microdata/">WPSSO Strip Schema Microdata</a></li>
<li><a href="https://wordpress.org/plugins/wpsso-tweet-a-quote/">WPSSO Tweet a Quote</a></li>
<li><a href="https://wordpress.org/plugins/wpsso-user-locale/">WPSSO User Locale</a></li>
</ul>
</blockquote>

<h4>Quick List of Features</h4>

<p><strong>NextGEN Facebook (NGFB) Free / Basic Features</strong></p>

<ul>
<li>Adds Open Graph meta tags for Facebook, Google+, Pinterest, LinkedIn, etc.</li>
<li>Adds Twitter Card meta tags:

<ul>
<li><a href="https://dev.twitter.com/cards/types/summary-large-image">Summary Card with Large Image</a></li>
<li><a href="https://dev.twitter.com/cards/types/summary">Summary Card</a></li>
<li><a href="https://dev.twitter.com/cards/types/player">Player Card</a> is available in the Pro version (requires video discovery modules, provided with the Pro version).</li>
</ul></li>
<li>Adds Weibo meta tags for article creation and update dates.</li>
<li>Adds Pinterest Rich Pin meta tags and Schema <a href="https://developers.google.com/search/docs/guides/intro-structured-data">Structured Data</a> markup for Google.</li>
<li>Adds author (Person), publisher (Organization) and WebSite markup for Google Search and <em>Knowledge Graph</em>, including markup for the <a href="https://developers.google.com/search/docs/data-types/sitelinks-searchbox">Google Sitelinks Searchbox</a>.</li>
<li>Customizable image dimensions for Facebook / Open Graph, Pinterest, Schema and all Twitter Card types.</li>
<li>Auto-regeneration of inaccurate / missing WordPress image sizes.</li>
<li>Support for Automattic's <a href="https://wordpress.org/plugins/amp/">Accelerated Mobile Pages (AMP)</a> plugin.</li>
<li>Support for featured, attached, gallery shortcode, and/or image HTML tags in content.</li>
<li>Fallback to image alt values if the content and excerpt does not include any text.</li>
<li>Optional fallback to a default image and video for index and search webpages.</li>
<li>Configurable title separator character (hyphen by default).</li>
<li>Configurable title and description lengths (Open Graph, Twitter Card, SEO).</li>
<li>Includes author, co-author / contributor and publisher markup for Facebook, Pinterest and Google.</li>
<li>Includes WordPress tags as hashtags (including a configurable maximum).</li>
<li>Includes a Google / SEO description meta tag (if an SEO plugin is not detected).</li>
<li>Configurable website / business social accounts for Schema markup:

<ul>
<li>Facebook Business Page URL</li>
<li>Google+ Business Page URL</li>
<li>Pinterest Company Page URL</li>
<li>Twitter Business @username</li>
<li>Instagram Business URL</li>
<li>LinkedIn Company Page URL</li>
<li>MySpace Business (Brand) URL</li>
</ul></li>
<li>User profile contact fields for Open Graph, Twitter Card, and Schema markup:

<ul>
<li>Facebook URL</li>
<li>Google+ URL</li>
<li>Instagram URL</li>
<li>LinkedIn URL</li>
<li>MySpace URL</li>
<li>Pinterest URL</li>
<li>Skype Username</li>
<li>Tumblr URL</li>
<li>Twitter @username</li>
<li>YouTube Channel URL</li>
</ul></li>
<li>Customizable <strong>multilingual</strong> / multi-language Site Title and Site Description texts.</li>
<li>Contextual help for <em>every</em> plugin option and <a href="https://surniaulula.com/codex/plugins/nextgen-facebook/">comprehensive online documentation</a>.</li>
<li>Uses object and transient caches to provide incredibly fast execution speeds.</li>
<li>Include / exclude each social share button based on the viewing device (desktop and/or mobile).</li>
<li>Social share buttons for the content, excerpt, in a widget, as a shortcode, floating sidebar, and/or PHP function.

<ul>
<li>Buffer</li>
<li>Email</li>
<li>Facebook</li>
<li>Google+</li>
<li>LinkedIn</li>
<li>ManageWP</li>
<li>Pinterest</li>
<li>Reddit</li>
<li>StumbleUpon</li>
<li>Tumblr</li>
<li>Twitter</li>
<li>WhatsApp (for Mobile Devices)</li>
</ul></li>
</ul>

<blockquote>
<p>Download the Free version from <a href="https://surniaulula.github.io/nextgen-facebook/">GitHub</a> or <a href="https://wordpress.org/plugins/nextgen-facebook/">WordPress.org</a>.</p>
</blockquote>

<div style="clear:both;"></div>

<div style="text-align:center;width=100%;max-width:1200px;height:auto;margin:0 0 20px 0;">
<p><img src="https://surniaulula.github.io/nextgen-facebook/images/social/twitter-player-card-single.png" width="100%" height="100%"/></p>
</div>

<h4>Quick List of Features (Continued)</h4>

<p><strong>NextGEN Facebook (NGFB) Pro / Power-User Features</strong></p>

<ul>
<li>A Social Settings metabox for Post, Page, custom post type, user profile, and term / taxonomy (category and tag) editing pages &mdash; the Social Settings metbox allows you to customize the article topic, the shared title, the Open Graph / Facebook / Rich Pin, Google Search, and Twitter Card descriptions, along with the shared image and/or video.</li>
<li>Fully renders content (including shortcodes) for accurate description texts.</li>
<li>Support for embedded videos in content text (iframe and object HTML markup).</li>
<li>Additional Open Graph / Rich Pin meta tags for videos and ecommerce products.</li>
<li>Twitter <a href="https://dev.twitter.com/cards/types/player">Player Card</a> markup for embedded videos from Facebook, Slideshare, Vimeo, Wistia, and/or Youtube.</li>
<li>Include or exclude individual Google / SEO, Open Graph, Twitter Card, and Schema meta tags in the webpage head section.</li>
<li>Validation of minimum / maximum image dimensions and aspect ratios.</li>
<li>Configurable user profile contact field names and labels.</li>
<li>Optional URL shortening with Bitly, Google, Ow.ly, TinyURL, or YOURLS.</li>
<li>Dynamic button language switching based on the current WordPress locale.</li>
<li>A stylesheets editor for each social share button locations (content, excerpt, shortcode, widget, etc.).</li>
<li>File caching for social share button images and JavaScript, maximizing performance on VPS and dedicated hardware hosting platforms.</li>
<li>Selection of preset button options by location (content, excerpt, shortcode, widget, etc.).</li>
<li>Ability to include / exclude social share buttons by post type.</li>
<li>Integrates with 3rd party plugins and services for additional image, video, ecommerce product details, SEO settings, etc. The following modules are included with the Pro version, and are automatically loaded if/when the supported plugins and/or services are required.

<ul>
<li><strong>Integrated 3rd Party Plugins</strong>

<ul>
<li>All in One SEO Pack</li>
<li>bbPress</li>
<li>BuddyPress (including Group Forum Topics)</li>
<li>Co-Authors Plus (including Guest Authors)</li>
<li>Easy Digital Downloads</li>
<li>HeadSpace2 SEO</li>
<li>NextGEN Gallery</li>
<li>MarketPress - WordPress eCommerce</li>
<li>Polylang</li>
<li>rtMedia for WordPress, BuddyPress and bbPress</li>
<li>The Events Calendar</li>
<li>The SEO Framework</li>
<li>WooCommerce (version 1 and 2)</li>
<li>WP eCommerce</li>
<li>WordPress REST API (version 2)</li>
<li>Yoast SEO (aka WordPress SEO)</li>
<li>Yotpo Social Reviews for WooCommerce</li>
</ul></li>
<li><strong>Integrated Service APIs</strong>

<ul>
<li>Bitly</li>
<li>Facebook Embedded Videos</li>
<li>Google URL Shortener</li>
<li>Gravatar (Author Image)</li>
<li>Ow.ly</li>
<li>Slideshare Presentations</li>
<li>TinyURL</li>
<li>Vimeo Videos</li>
<li>Wistia Videos</li>
<li>Your Own URL Shortener (YOURLS)</li>
<li>YouTube Videos and Playlists</li>
</ul></li>
</ul></li>
</ul>

<blockquote>
<p><a href="https://surniaulula.com/extend/plugins/nextgen-facebook/?utm_source=ngfb-readme-purchase">Purchase the Pro version</a> (includes a <em>No Risk 30 Day Refund Policy</em>).</p>
</blockquote>

<div style="clear:both;"></div>

<div style="text-align:center;width=100%;max-width:1200px;height:auto;margin:0 0 20px 0;">
<p><img src="https://surniaulula.github.io/nextgen-facebook/images/settings/ngfb-social-sharing-buttons.png" width="100%" height="100%"/></p>
</div>

<h4>Social Sharing Buttons</h4>

<p>NextGEN Facebook (NGFB) comes with several social share buttons, that you can optionally include on Post / Page editing pages, above / below your content or excerpt, <a href="https://wordpress.org/plugins/bbpress/">bbPress</a> single pages, <a href="https://wordpress.org/plugins/buddypress/">BuddyPress</a> activity entries, as a sidebar, widget, shortcode, or even call a function from your theme template(s). Each of the following social share buttons can be enabled, configured, and styled individually:</p>

<ul>
<li><strong>Email</strong></li>
<li><strong>Buffer</strong></li>
<li><strong>Facebook</strong> (Like, Send, and Share)</li>
<li><strong>Google+</strong></li>
<li><strong>LinkedIn</strong></li>
<li><strong>ManageWP</strong></li>
<li><strong>Pinterest</strong></li>
<li><strong>Reddit</strong></li>
<li><strong>StumbleUpon</strong></li>
<li><strong>Tumblr</strong> (Links, Quotes, Images, Videos)</li>
<li><strong>Twitter</strong></li>
<li><strong>WhatsApp</strong> (for Mobile Devices)</li>
</ul>

<p>The Facebook, Google+ and Twitter social share buttons support <em>multiple languages</em>. A default language can be chosen in the NGFB settings, and the <a href="https://surniaulula.com/extend/plugins/nextgen-facebook/">Pro version</a> switches the social share button language with the webpage language / WordPress locale. NGFB can also include hashtags from WordPress and NextGEN Gallery tag names in the Open Graph (Facebook) and Pinterest Rich Pin descriptions, Tweet text, and other social captions.</p>

<div style="clear:both;"></div>

<div style="text-align:center;width=100%;max-width:1200px;height:auto;margin:0 0 20px 0;">
<p><img src="https://surniaulula.github.io/nextgen-facebook/images/social/google-social-profiles-search-results.png" width="100%" height="100%"/></p>
</div>

<h4>Social Profiles for Google Search</h4>

<p>NextGEN Facebook (NGFB) includes support for <a href="https://developers.google.com/structured-data/customize/social-profiles">author (Person) and publisher (Organization) social profiles</a>, <a href="https://developers.google.com/structured-data/customize/logos">publisher (Organization) logo</a>, and <a href="https://developers.google.com/structured-data/site-name">WebSite site name</a> in Schema JSON-LD format for Google Search and their <em>Knowledge Graph</em>. The author (Person) markup includes the author's website URL, their social sharing image, and contact URLs. The publisher (Organization) markup includes the website URL, a corporate logo, the website default image, and the publisher Facebook, Google+, LinkedIn, Pinterest, and Twitter business pages.</p>

<div style="clear:both;"></div>

<h4>User Profile Social Contacts</h4>

<p>NextGEN Facebook (NGFB) Pro allows you to customize the field names, label, and add / remove the following contacts on user profile pages and <a href="https://wordpress.org/plugins/co-authors-plus/">Co-Authors Plus</a> guest author profiles:</p>

<ul>
<li>AIM</li>
<li>Facebook URL</li>
<li>Google Talk</li>
<li>Google+ URL</li>
<li>Instagram URL</li>
<li>LinkedIn URL</li>
<li>MySpace URL</li>
<li>Pinterest URL</li>
<li>Skype Username</li>
<li>Tumblr URL</li>
<li>Twitter @username</li>
<li>Yahoo IM</li>
<li>YouTube Channel URL</li>
</ul>

<h4>Complete Social Meta Tags</h4>

<p>NextGEN Facebook (NGFB) adds Facebook / Open Graph, Pinterest Rich Pins, Twitter Cards, and Search Engine Optimization (SEO) meta tags to the head section of webpages. These meta tags are used by Google Search and all social websites to describe and display your content correctly (title, description, hashtags, images, videos, ecommerce product details, author profile / authorship, publisher, etc.). NGFB uses the <em>existing</em> content of your webpages to build HTML meta tags &mdash; there's no need to manually enter / configure any additional values or settings (although many settings and options <em>are</em> available in the Pro version). <a href="https://surniaulula.com/extend/plugins/nextgen-facebook/screenshots/">See example screenshots from Google Search, Google+, Facebook, Twitter, Pinterest, StumbleUpon, Tumblr, etc.</a>.</p>

<p>NextGEN Facebook (NGFB) provides the <a href="https://dev.twitter.com/cards/types/summary">Summary</a>, <a href="https://dev.twitter.com/cards/types/summary-large-image">Summary with Large Image</a>, and <a href="https://dev.twitter.com/cards/types/player">Player</a> Twitter Cards &mdash; <em>including configurable image sizes for each card type</em>.</p>

<h4>3rd Party Integration (Pro version)</h4>

<div style="clear:both;"></div>

<div style="text-align:center;width=100%;max-width:1200px;height:auto;margin:0 0 20px 0;">
<p><img src="https://surniaulula.github.io/nextgen-facebook/images/social/twitter-large-image-summary-single.png" width="100%" height="100%"/></p>
</div>

<p><strong>Images and Videos</strong></p>

<p>NextGEN Facebook (NGFB) detects and uses all images - associated or included in your content - including the WordPress image gallery and <a href="https://wordpress.org/plugins/nextgen-gallery/">NextGEN Gallery</a> shortcodes. WordPress Media Library images (and NextGEN Gallery in the Pro version) are resized according to their intended consumer (Facebook, Twitter, Pinterest, etc). The Pro version detects embedded videos from Facebook, Slideshare, Vimeo, Wistia, and Youtube (including preview image, video title, and video description). NGFB (Pro version) also includes support for <a href="https://wordpress.org/plugins/nextgen-gallery/">NextGEN Gallery</a> albums, galleries and images (shortcodes, image tags, album / gallery preview images, etc.).</p>

<p><strong>Enhanced SEO</strong></p>

<p>NextGEN Facebook (NGFB) Pro integrates with <a href="https://wordpress.org/plugins/all-in-one-seo-pack/">All in One SEO Pack</a>, <a href="https://wordpress.org/plugins/headspace2/">HeadSpace2 SEO</a>, <a href="https://wordpress.org/plugins/autodescription/">The SEO Framework</a>, and <a href="https://wordpress.org/plugins/wordpress-seo/">Yoast SEO</a> (aka WordPress SEO) &mdash; making sure your custom SEO settings are reflected in the Open Graph, Rich Pin, Schema Structured Data, and Twitter Card meta tags.</p>

<div style="clear:both;"></div>

<div style="text-align:center;width=100%;max-width:1200px;height:auto;margin:0 0 20px 0;">
<p><img src="https://surniaulula.github.io/nextgen-facebook/images/social/twitter-product-ngfb-single.png" width="100%" height="100%"/></p>
</div>

<p><strong>eCommerce Products</strong></p>

<p>NextGEN Facebook (NGFB) Pro also supports <a href="https://wordpress.org/plugins/easy-digital-downloads/">Easy Digital Downloads</a>, <a href="https://wordpress.org/plugins/wordpress-ecommerce/">MarketPress - WordPress eCommerce</a>, <a href="https://wordpress.org/plugins/woocommerce/">WooCommerce</a>, and <a href="https://wordpress.org/plugins/wp-e-commerce/">WP e-Commerce</a> product pages, creating appropriate meta tags for [Facebook Products](https://developers.facebook.com/docs/payments/product/, and <a href="http://developers.pinterest.com/rich_pins/">Pinterest Rich Pins</a>, including variations and additional / custom images.</p>

<p><strong>Forums and Social</strong></p>

<p>NextGEN Facebook (NGFB) Pro supports <a href="https://wordpress.org/plugins/bbpress/">bbPress</a>, <a href="https://wordpress.org/plugins/buddypress/">BuddyPress</a> (see the <a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/buddypress-integration/">BuddyPress Integration Notes</a>), and <a href="https://wordpress.org/plugins/buddypress-media/">rtMedia for WordPress, BuddyPress and bbPress</a>, making sure your meta tags reflect the page content, including appropriate titles, descriptions, images, author information, etc. Social share buttons can also be added to <a href="https://wordpress.org/plugins/bbpress/">bbPress</a> single template pages and <a href="https://wordpress.org/plugins/buddypress/">BuddyPress</a> activities.</p>

<h4>Proven Performance</h4>

<p>NextGEN Facebook (NGFB) is <em>fast and coded for performance</em>, making full use of all available caching techniques (persistent / non-persistent object and disk caching). NGFB loads only the library files and object classes it needs, keeping it small, fast, and yet still able to support a wide range of 3rd party integration features. NGFB requires PHP v5.2.0 or better, and is fully compatible with PHP v7.</p>

<h4>Professional Support</h4>

<p>NextGEN Facebook (NGFB) support and development is on-going. You can review the <a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/">FAQ</a> and <a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/">Notes</a> pages for additional setup information. If you have any suggestions or comments, post them to the <a href="https://wordpress.org/support/plugin/nextgen-facebook">WordPress support forum</a> or the <a href="http://nextgen-facebook.support.surniaulula.com/">Pro version support website</a>.</p>

<p>Follow Surnia Ulula on <a href="https://plus.google.com/+SurniaUlula/?rel=author">Google+</a>, <a href="https://www.facebook.com/SurniaUlulaCom">Facebook</a>, <a href="https://twitter.com/surniaululacom">Twitter</a>, and <a href="http://www.youtube.com/user/SurniaUlulaCom">YouTube</a>.</p>


<h2>Installation</h2>

<h4>Install and Uninstall</h4>

<ul>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/installation-to/install-the-plugin/">Install the Plugin</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/installation/integration/">Integration Notes</a>

<ul>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/installation/integration/buddypress-integration/">BuddyPress Integration</a></li>
</ul></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/installation/uninstall-the-plugin/">Uninstall the Plugin</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/installation/performance-tuning/">Performance Tuning</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/installation/debugging-and-problem-solving/">Debugging and Problem Solving</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/installation/developer-special-buy-one-get-one-free/">Developer Special - Buy one, Get one Free</a></li>
</ul>

<h4>Plugin Setup</h4>

<ul>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/installation/a-setup-guide/">A Setup Guide for NGFB</a></li>
</ul>


<h2>Frequently Asked Questions</h2>

<h4>Frequently Asked Questions</h4>

<ul>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/can-i-use-the-pro-version-on-multiple-websites/">Can I use the Pro version on multiple websites?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/does-linkedin-read-the-open-graph-meta-tags/">Does LinkedIn read the Open Graph meta tags?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/doesnt-an-seo-plugin-cover-that/">Doesn't an SEO plugin cover that?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-exclude-certain-parts-of-the-content-text/">How can I exclude / ignore certain parts of the content text?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-fix-a-err_too_many_redirects-error/">How can I fix a ERR_TOO_MANY_REDIRECTS error?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-fix-an-http-error-when-uploading-images/">How can I fix an "HTTP error" when uploading images?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-have-smaller-dimensions-for-the-default-image/">How can I have smaller dimensions for the default image?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-see-what-the-facebook-crawler-sees/">How can I see what the Facebook crawler sees?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-share-a-single-nextgen-gallery-image/">How can I share a single NextGEN Gallery image?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-do-i-attach-an-image-without-showing-it-on-the-webpage/">How do I attach an image without showing it on the webpage?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-do-i-fix-my-themes-front-page-pagination/">How do I fix my theme's front page pagination?</a></li>
<li><a href="https://wpsso.com/codex/plugins/wpsso/faq/how-do-i-install-the-ngfb-pro-version/">How do I install the NGFB Pro version?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-do-i-remove-duplicate-meta-tags/">How do I remove duplicate meta tags?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-does-ngfb-find-detect-select-images/">How does NGFB find / detect / select images?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/">Social Sharing Buttons</a>

<ul>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/can-i-share-a-single-image-on-a-webpage/">Can I share a single image on a webpage?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/can-i-use-other-social-sharing-buttons/">Can I use other social sharing buttons?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/how-do-i-turn-on-social-sharing-buttons-for-a-page/">How do I turn on Social Sharing Buttons for a page?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/why-are-the-buttons-showing-the-wrong-language/">Why are the buttons showing the wrong language?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/why-does-the-facebook-like-button-flyout-get-clipped/">Why does the Facebook "Like" button flyout get clipped?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/why-doesnt-the-pinterest-button-show/">Why doesn't the Pinterest button show?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/why-doesnt-the-twitter-count-increase/">Why doesn't the Twitter count increase?</a></li>
</ul></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/w3c-says-there-is-no-attribute-property/">W3C says "there is no attribute 'property'"</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/what-about-google-search-and-google-plus/">What about Google Search and Google Plus?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/what-features-of-nextgen-gallery-are-supported/">What features of NextGEN Gallery are supported?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/what-is-the-difference-between-ngfb-and-wpsso/">What is the difference between NGFB and WPSSO?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/what-is-the-difference-between-the-free-and-pro-versions/">What is the difference between the Free and Pro versions?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-arent-pins-from-my-website-posting-rich/">Why aren't Pins from my website posting Rich?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-do-my-facebook-shares-have-small-images/">Why do my Facebook shares have small images?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-does-facebook-play-videos-instead-of-linking-them/">Why does Facebook play videos instead of linking them?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-does-facebook-show-the-wrong-image-text/">Why does Facebook show the wrong image / text?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-does-google-structured-data-testing-tool-show-errors/">Why does Google Structured Data Testing Tool show errors?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-does-ngfb-ignore-some-img-html-tags/">Why does the plugin ignore some &lt;img/&gt; HTML tags?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-dont-my-twitter-cards-show-on-twitter/">Why don't my Twitter Cards show on Twitter?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-is-the-open-graph-title-the-same-for-every-webpage/">Why is the Open Graph title the same for every webpage?</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-is-the-page-blank-or-its-components-misaligned/">Why is the page blank or its components misaligned?</a></li>
</ul>


<h2>Other Notes</h2>

<h3>Other Notes</h3>
<h4>Additional Documentation</h4>

<ul>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/">About NGFB Pro Integration Modules</a>

<ul>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/author-gravatar/">Author Gravatar</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/easy-digital-downloads/">Easy Digital Downloads</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/facebook-slideshare-vimeo-wistia-youtube-video-apis/">Facebook, Slideshare, Vimeo, Wistia, Youtube Video APIs</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/headspace2-seo/">HeadSpace2 SEO</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/woocommerce/">WooCommerce</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/wordpress-rest-api-v2/">WordPress REST API version 2</a></li>
</ul></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/contact-information/">Contact Information and Feeds</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/">Developer Resources</a>

<ul>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/constants/">Constants</a>

<ul>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/constants/disable-the-open-graph-meta-tags/">Disable the Open Graph Meta Tags</a></li>
</ul></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/">Filters</a>

<ul>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/">Filter Examples</a>

<ul>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/add-schema-aggregaterating-meta-tags/">Add Schema aggregateRating Meta Tags</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/detect-youtube-url-links-as-videos/">Detect YouTube URL Links as Videos</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/modify-shortcode-attributes-url/">Modify Shortcode Attributes (URL)</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/modify-the-articletag-keywords-names/">Modify the "article:tag" Keywords / Names</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/modify-the-default-topics-list/">Modify the Default Article Topics List</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/modify-the-home-page-title-for-facebook-open-graph/">Modify the Home Page Title for Facebook / Open Graph</a></li>
</ul></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/">Filters by Category</a>

<ul>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/head/">Head Filters</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/media/">Media Filters</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/open-graph/">Open Graph Filters</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/twitter-card/">Twitter Card Filters</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/webpage/">Webpage Filters</a></li>
</ul></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-name/">Filters by Name</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/other/">Other Filters</a></li>
</ul></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/sharing-buttons-function/">Sharing Buttons Function</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/the-mod-variable/">The $mod Variable</a></li>
</ul></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/inline-variables/">Inline Variables</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/multisite-network-support/">Multisite / Network Support</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/ngfb-shortcode/">NGFB Shortcode for Sharing Buttons</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/styling-social-buttons/">Styling Social Buttons</a></li>
<li><a href="https://surniaulula.com/codex/plugins/nextgen-facebook/notes/working-with-image-attachments/">Working with Image Attachments</a></li>
</ul>

