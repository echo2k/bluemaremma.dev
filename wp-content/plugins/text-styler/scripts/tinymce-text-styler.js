(function($) {
    var $passed = text_styler_data;
    var $wp_url = $passed.wp_url;
    var $plugin_url = $passed.plugin_url;
    var is_style_added = false;
    
    tinymce.create('tinymce.plugins.text_styler_plugin', {
        init: function(editor, url) {                        
            title = 'Text Styler';
            editor.addButton('text_styler_button', {
                title: title,
                image: $plugin_url + '/images/ts-mce-button.png',
                cmd: 'text_styler_command',
                onPostRender: function() {
                    var _this = this;   // reference to the button itself
                    editor.on('NodeChange', function(e) {
                        i=tinyMCE.activeEditor;
                        l=i.dom;
                        j=i.selection;
                        list = l.getParent(j.getNode(),"button");
                        node = i.selection.getNode();
                        id = node.id;
                        id_split = id.split('-');   
                        
                        parent = l.getParent(j.getNode(), 'SPAN');
                        if(parent) {
                            parent_id = parent.id;
                            parent_id_split = parent_id.split('-');  
                            is_styled_text = (parent_id_split[0] == 'st') ? true : false;
                        }
                        else {
                            is_styled_text = false;
                        }
                
                        if(id_split[0] == 'st' || is_styled_text) {
                            _this.active(true);
                        }
                        else {
                            _this.active(false);
                        }
                        
                        if (!is_style_added) {
                            // loads the custom styles of the current post to the tinymce editor
                            $(tinyMCE.activeEditor.dom.select('head')).append('<style type="text/css" id="custom-text-styles">' + text_styler_data.styles + '</style>');                            
                            is_style_added = true;
                        }
                        
                    });
                }
            });

            editor.addCommand('text_styler_command', function() {
                i=tinyMCE.activeEditor;
                l=i.dom;
                j=i.selection;
                node = i.selection.getNode();
                id = node.id;
                id_split = id.split('-');   

                $p = l.getParents(j.getNode());

                $opt = '<div><input type="radio" class="new-el" checked="checked" name="element" value="NEW_EL"> New Element (SPAN)';
                $opt += '<div class="el-option"><textarea style="width: 100%; height: 30px; font-size: 13px;" name="text">enter text here</textarea></div>';

                for($x in $p) {
                    $node_name = $p[$x].nodeName;
                    if (!$p[$x].id) {
                        $p[$x].id = 'st-' + $node_name + '-' + Date.now();
                    }
                    if ($p[$x].nodeName != 'BODY' && $p[$x].nodeName != 'HTML') {
                        $opt += '<br /><input class="' + $p[$x].nodeName + '" type="radio" name="element" value="' + $p[$x].id + '"> ' + $p[$x].nodeName;
                        $opt += '<div class="' + $p[$x].id + ' el-option">' + $($p[$x]).html() + '</div>';
                    }
                }

                $opt += '</div>';

                if(id_split[0] == 'st') {
                    $value = $(node).html();         
                    showWindow(editor, $value, null, $opt, text_styler_data.post_id, url, text_styler_data.ajax_url, $wp_url);
                }
                else {                    
                    $value = editor.selection.getContent();            
                    showWindow(editor, $value, $p, $opt, text_styler_data.post_id, url, text_styler_data.ajax_url, $wp_url);
                }
            });        
        }
    });

    tinymce.PluginManager.add('text_styler_plugin', tinymce.plugins.text_styler_plugin);
    
    function showWindow(editor, $value, $p,$opt, $post_id, $url, $ajax_url, $wp_url) {
        editor.windowManager.open(
            //  Properties of the window.
            {
                title: title,
                file:  $url + '/pop-up-text-styler.php?wp_url=' + $wp_url,
                width: 600,
                height: 500,
                inline: 1
            },
            {
                editor: editor,
                jquery: $,
                value: $value,
                parents : $p,
                options: $opt,
                ajax_url: $ajax_url,
                post_id:  $post_id
            }
        );    
    }    
})(jQuery);