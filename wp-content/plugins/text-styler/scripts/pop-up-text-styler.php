<?php
include_once('../TXST/MCEForm.php');
include_once('../TXST/MCEForm/StyleOptions.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <!-- Disable browser caching of dialog window -->
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="pragma" content="no-cache" />
        
        <link rel='stylesheet' href='<?php echo $_GET['wp_url'] ?>/wp-admin/load-styles.php?c=1&amp;dir=ltr&amp;load%5B%5D=wp-color-picker' type='text/css' media='all' />
        <link rel='stylesheet' href='../styles/tinymce-color-picker.css' type='text/css' media='all' />        
        <link rel='stylesheet' href='../styles/tinymce-text-styler.css' type='text/css' media='all' />        
        <style type='text/css'>
          
	</style>
    </head>
    <body>
        <div id="text_styler_dialog_wrapper">
                <div class="help">Need help? Click <a href="http://wordpress.org/support/plugin/text-styler" target="_blacnk">here</a> to submit a ticket! Also check out this <a href="https://www.youtube.com/watch?v=Ev1JMmkp9wU&feature=youtu.be" target="_blank">video tutorial</a> on how to use Text Styler.</div>
                <div class="step1">
                    
                    <div class="frm-buttons">
                        <input type="button" value="Cancel" id="cancel">
                        <input type="button" value="Continue" id="continue-step2" />
                    </div>                     
                </div>               
                <div class="step2">
                    <?php
                    $form = new TXST_MCEForm_StyleOptions();
                    $form->showForm();
                    ?>
            </div>
        </div>

        <script type="text/javascript">
            var passed_arguments = top.tinymce.activeEditor.windowManager.getParams();
            var $ = passed_arguments.jquery;
            var jq_context = document.getElementsByTagName("body")[0];

            var $field_types = new Array();
            $field_types['color'] = 'color-picker';
            $field_types['border-color'] = 'color-picker';
            $field_types['background-color'] = 'color-picker';
            $field_types['font-family'] = 'dropdown';
            $field_types['border-style'] = 'dropdown';
            $field_types['font-size'] = 'text';
            $field_types['border-width'] = 'text';
            $field_types['margin'] = 'text';
            $field_types['padding'] = 'text';
            $field_types['border-radius'] = 'text';
            $field_types['font-style'] = 'radio';
            $field_types['font-weight'] = 'radio';

            
            $('#continue-step2', jq_context).live('click', function(){           
                $id = $('input[name="element"]:checked', jq_context).val();
                if ($('input[name="element"]:checked', jq_context).hasClass('UL') || 
                    $('input[name="element"]:checked', jq_context).hasClass('OL')) {
                    $('#css-list', jq_context).css('display', 'block');
                }
                $.ajax({
                    url: passed_arguments.ajax_url, //AJAX file path – admin_url("admin-ajax.php")
                    type: "POST",
                    data: 'action=get_styles&id=' + $id + '&post_id=' + passed_arguments.post_id,
                    dataType: "json",
                    success: function($data){
                        jq_context = document.getElementsByTagName("body")[0]
                        if ($data.success) {   
                            for($i in $data.styles) {
                                switch ($field_types[$i]) {
                                    case 'color-picker':
                                        $('div.field-' + $i + ' .std-input', jq_context).wpColorPicker('color', $data.styles[$i]);
                                        break;
                                    case 'text':
                                        $('div.field-' + $i + ' .std-input', jq_context).val($data.styles[$i]);
                                        break;     
                                    case 'dropdown':
                                        if ($data.styles[$i]) {
                                            $('div.field-' + $i + ' select', jq_context).val($data.styles[$i]);
                                        }
                                        break;                                                                                
                                    case 'checkbox':
                                        if ($data.styles[$i]) {
                                            $('div.field-' + $i + ' input', jq_context).attr('checked', 'checked');
                                        }                                        
                                        break;
                                    case 'radio':
                                        $('div.field-' + $i + ' input', jq_context).removeAttr('checked');
                                        if ($data.styles[$i]) {
                                            $('div.field-' + $i + ' input[value="' + $data.styles[$i] + '"]', jq_context).attr('checked', 'checked');
                                        }                                        
                                        break;                                        
                                }
                            }
                        }
                    }
                }, jq_context);
                
                $('.step1', jq_context).css('display','none');
                $('.step2', jq_context).css('display','block');
            });

            $("form", jq_context).submit(function(event) {
                event.preventDefault();
                var input_text = $("textarea[name='text']", jq_context).val();
                $form = $(this, jq_context).serialize();

                $id = $('input[name="element"]:checked', jq_context).val();

                if ($id == 'NEW_EL') {
                    $id = $new_id = 'st-SPAN-' + Date.now();
                    
                    $text = '<span id="' + $id + '">' + (( input_text) ?  input_text : 'text') + '</span>';
                    passed_arguments.editor.selection.setContent($text);                    
                }
                
                $.ajax({
                    url: passed_arguments.ajax_url, //AJAX file path – admin_url("admin-ajax.php")
                    type: "POST",
                    data: 'action=save_styles&id=' + $id + '&post_id=' + passed_arguments.post_id + '&' + $form,
                    dataType: "json",
                    success: function($data){
                        $(passed_arguments.editor.dom.select('style#custom-text-styles'), jq_context).remove();
                        $(passed_arguments.editor.dom.select('head'), jq_context).append('<style type="text/css" id="custom-text-styles">' + $data.styles + '</style>');
                        
                        passed_arguments.editor.windowManager.close();
                    },
                    error: function($data, $textStatus, $errorThrown){
                        
                    }
                }, jq_context);                                  
            });

            $('#cancel', jq_context).live('click', function(){
                passed_arguments.editor.windowManager.close();
            });
                    

            $('.step1', jq_context).prepend(passed_arguments.options);
            $('.wp-color-picker', jq_context).wpColorPicker();
            $('.od-color-picker', jq_context).wpColorPicker();            
            $('.date-picker', jq_context).datepicker();
            //$('.tinymce-tabs', jq_context).tabs();
            
            $('.tinymce-tabs', jq_context).each(function(){
                $href = $(this, jq_context).find('li.active a').attr('href');
                $tab_id = $href.substr(1);
                $('#'+$tab_id, jq_context).css('display', 'block');
            });
            
            $('.tinymce-tabs .tab-links li a', jq_context).live('click', function(){                
                $('.tinymce-tabs .tab-links li', jq_context).removeClass('active');                
                $(this, jq_context).parents('li').addClass('active');
                
                $('.tinymce-tabs .tab-content .tab', jq_context).css('display', 'none');
                $href = $(this, jq_context).attr('href');
                $tab_id = $href.substr(1);
                $('#'+$tab_id, jq_context).css('display', 'block');
                
                return false;
            });
        </script>
    </body>
</html>