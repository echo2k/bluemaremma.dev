<?php
/*
 * Plugin Name: Text Styler
 * Description: This plugin will allow a user to style text/phrase of a post or page. He can set background color, text color, and padding, border, etc. Easily adds styles to all tags such as header,paragraph, list, list item, span, strong, etc.
 * Author: EdesaC
 * Version: 1.0.6
**/

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

include_once('TXST/MCEForm.php');
include_once('TXST/MCEForm/StyleOptions.php');

TXST_MCEForm::_init();

class TextStyler {
    protected $_plugin_url;
    protected $_plugin_folder;

    public function __construct($file) {
        if (!$file) {
            throw new Error('Missing 1 argument!');
        }

        $this->_plugin_url = plugin_dir_url($file);
        $this->_plugin_folder = basename(dirname($file));

        add_action('admin_enqueue_scripts', array($this, 'load_admin_scripts_css'));
        add_action('wp_footer', array($this, 'load_website_scripts_css'));

        add_action( 'admin_head', array($this, 'saveTinymceData'));
        add_filter( 'mce_buttons', array($this, 'registerTinymceButton'));
        add_filter( 'mce_external_plugins', array($this, 'registerTinymcePlugin'));

        add_action('wp_ajax_get_styles', array($this, 'ajaxGetStyles'));
        add_action('wp_ajax_nopriv_get_styles', array($this, 'ajaxGetStyles'));

        add_action('wp_ajax_save_styles', array($this, 'ajaxSaveStyles'));
        add_action('wp_ajax_nopriv_save_styles', array($this, 'ajaxSaveStyles'));
        
        add_filter('the_content', array($this, 'filterContent'));
        
    }

    /**
     * save the customs styles of a post to the database
     */
    public function ajaxSaveStyles() {
        $data = $_POST;
        
        $post_id = $data['post_id'];
        $css_id = $data['id'];
        $post_styles = get_post_meta($post_id, 'text_styles', true);
        
        $post_styles[$css_id] = $data['data'];
        update_post_meta($post_id, 'text_styles', $post_styles);   

        $new_styles = $this->getStyles($post_id);
        $ret['styles'] = $new_styles;
        $ret['post_id'] = $data['post_id'];
        $ret['success'] = true;

        print_r( json_encode($ret));    
        exit;
    }
    
    /**
     * return the customs of a given post
     */
    public function ajaxGetStyles() {
        $data = $_POST;
        $post_id = $data['post_id'];
        $css_id = $data['id'];

        $defaults = array('color' => '', 'background-color' => '', 'padding' => '');
        $post_styles = get_post_meta($post_id, 'text_styles', true);

        if (is_array($post_styles) && isset($post_styles[$css_id])) {
            $final_styles = array_merge($defaults, $post_styles[$css_id]);
        }
        else {
            $final_styles = $defaults;
        }

        $ret['styles'] = $final_styles;
        $ret['post_id'] = $data['post_id'];
        $ret['success'] = true;

        print_r( json_encode($ret));    
        exit;
    }

    /**
     * Get the custom styles of the post specified
     * @param type $post_id the id of the post
     * @return string e.g. #id1 {color: red} #id2 {background: green}
     */
    public function getStyles($post_id) {
        $post_styles = get_post_meta($post_id, 'text_styles', true);        
        $allowed_styles = array('padding', 'margin', 'color', 'background-color', 'font-family', 'font-size', 'font-weight', 'font-style', 'line-height', 'border-style', 'border-width', 'border-color', 'border-radius', 'list-style-position', 'list-style-type', 'font-style', 'font-weight');
        
        if (is_array($post_styles)) {
            $s = '.ts ul, .ts ol {padding: 0; margin: 0;}';
            foreach ($post_styles as $id => $styles) {            
                if (strpos($id, 'st-') === 0) {
                    $s .= "#" . $id . " { ";
                    foreach ($styles as $css => $value) {
                        if (in_array($css, $allowed_styles)) {
                            $s .= $css . ': ' . $value . ';';
                        }
                    }
                    $s .= " } ";

                    if (strpos($id, 'st-UL-') === 0 || strpos($id, 'st-OL-') === 0) {
                        $s .= "#" . $id . " > li{ ";
                        $s .= 'list-style-type: inherit;';
                        $s .= 'list-style-position: inherit;';
                        $s .= " } ";
                    }                
                }
            }
        }
        
        return $s;
    }
    
    public function registerTinymceButton( $button_array ) {
        global $current_screen;
        $type = $current_screen->post_type;

        if( is_admin() && ( $type == 'post' || $type == 'page' ) ) {
            array_push( $button_array, 'text_styler_button' );
        }

        return $button_array;
    }    

    public function registerTinymcePlugin( $plugin_array ) {
        global $current_screen;
        $type = $current_screen->post_type;

        if( is_admin() && ( $type == 'post' || $type == 'page' ) ) {
            $plugin_array['text_styler_plugin'] = $this->_plugin_url . 'scripts/tinymce-text-styler.js';
        }

        return $plugin_array;
    }
    
    public function saveTinymceData() {
        global $post;
        if (isset($_GET['post'])) {
            $post_id = $post->ID;
            $post_styles = $this->getStyles($post_id);
             ?>
             <script type='text/javascript'>
                var text_styler_data = {
                    'plugin_url': '<?php echo $this->_plugin_url; ?>',
                    'wp_url': '<?php echo get_bloginfo('wpurl'); ?>',
                    'ajax_url': '<?php echo admin_url('admin-ajax.php'); ?>',
                    'post_id': <?php echo $post->ID ?>,
                    'styles': '<?php echo $post_styles ?>'
                    };
             </script>
             <?php
        }
    }

    public function filterContent($content){
        return '<div ciass="ts">' . $content . '</div>';
    }
    
    public function load_website_scripts_css() {
        wp_enqueue_script($this->_plugin_folder . '-wp-scripts', $this->_plugin_url . '/scripts/wp-scripts.js');
        wp_enqueue_style($this->_plugin_folder . '-wp-styles', $this->_plugin_url . '/styles/wp-styles.css');
        
        global $post;
        $post_id = $post->ID;
        $s = $this->getStyles($post_id);
        
        // load the custom styles for the current post
        $styles = "<style type=\"text/css\">" . $s . "</style>";
        echo $styles;
    }

    public function load_admin_scripts_css() {
        wp_enqueue_media();
        wp_enqueue_script('jquery');

        wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_script('jquery-ui-sortable');
        wp_enqueue_script('jquery-ui-tabs');

        wp_enqueue_style( 'wp-color-picker' );

        wp_enqueue_script(
            'wp-color-picker',
            admin_url( 'js/color-picker.min.js'),
            array( 'iris' ),
            false,
            1
        );
    }
} // end of class

new TextStyler(__FILE__);

