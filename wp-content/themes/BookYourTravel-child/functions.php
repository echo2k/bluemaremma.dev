<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

        
if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css' );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css' );

// END ENQUEUE PARENT ACTION

if (!function_exists('bluemaremma_breadcrumbs')) {
    function bluemaremma_breadcrumbs($breadcrumb) {
        if(function_exists('bcn_display'))
        {
            echo '<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">';
            bcn_display();
            echo '</div>';
        }
        return '';
    }
}
add_filter( 'bookyourtravel_breadcrumbs', 'bluemaremma_breadcrumbs' );

// registra dimensione  immagine per feature list in home page
add_image_size( 'homepage-feat', 225, 254, true );

// invia mail dopo che un utente ha fatto la registrazione, usa l'action di PrivateContent
if ( ! function_exists('notifyAdminNewUser')) {
    function notifyAdminNewUser($user_id, $status) {
        
        global $pc_users;
        $result = $pc_users->get_users(array('user_id' => $user_id));
        $adminMail = get_option('admin_email');
        $res = wp_mail ( $adminMail, 
                  "Nuova registrazione a Blu E Maremma", 
                  "Una nuova agenzia ha fatto richiesta di registrazione su www.bluemaremmaviaggi.it\n
                  \n
                  Dati agenzia:\n
                  Nome: {$result[0]['name']} {$result[0]['surname']}\n
                  Email: {$result[0]['email']}\n
                  Telefono: {$result[0]['tel']}\n
                  \n
                  L'agenzia si trova in stato 'pending'
                  "
                  );
    }
}
add_action('pc_registered_user','notifyAdminNewUser');


add_action( 'wp_print_scripts', 'de_script', 100 );

function de_script() {
    wp_dequeue_script( 'wc-cart-fragments' );

    return true;
}

/**
 * Optimize WooCommerce Scripts
 * Remove WooCommerce Generator tag, styles, and scripts from non WooCommerce pages.
 */
add_action( 'wp_enqueue_scripts', 'child_manage_woocommerce_styles', 99 );
 
function child_manage_woocommerce_styles() {
 //remove generator meta tag
 remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );
 
 //first check that woo exists to prevent fatal errors
 if ( function_exists( 'is_woocommerce' ) ) {
 //dequeue scripts and styles
 if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
    wp_dequeue_style( 'woocommerce_frontend_styles' );
    wp_dequeue_style( 'woocommerce_fancybox_styles' );
    wp_dequeue_style( 'woocommerce_chosen_styles' );
    wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
    wp_dequeue_script( 'wc_price_slider' );
    wp_dequeue_script( 'wc-single-product' );
    wp_dequeue_script( 'wc-add-to-cart' );
    wp_dequeue_script( 'wc-cart-fragments' );
    wp_dequeue_script( 'wc-checkout' );
    wp_dequeue_script( 'wc-add-to-cart-variation' );
    wp_dequeue_script( 'wc-single-product' );
    wp_dequeue_script( 'wc-cart' );
    wp_dequeue_script( 'wc-chosen' );
    wp_dequeue_script( 'woocommerce' );
    wp_dequeue_script( 'prettyPhoto' );
    wp_dequeue_script( 'prettyPhoto-init' );
    wp_dequeue_script( 'jquery-blockui' );
    wp_dequeue_script( 'jquery-placeholder' );
    wp_dequeue_script( 'fancybox' );
    wp_dequeue_script( 'jqueryui' );
 }
 }
 
}

?>